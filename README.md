# landb-synchronizer

LanDB synchronizer is a simple python scrip that is responsible by fetching all Openshift/Kubernetes nodes
that match a given `labelSelector` and then add them to a lanDB set. If the set does not exist a new set will be
created. Nodes that do not appear in the query will also be removed from the lanDB set. This script also has the 
capability of removing a set from lanDB.

To run the script one can simply run:

```shell
docker run --rm gitlab-registry.cern.ch/paas-tools/okd4-deployment/landb-synchronizer:latest --kubeconfig ${KUBECONFIG} --command add --landb-set-name <set-name> --label-selector "" --landb-user ${USER} --landb-password ${PASSWORD} --description "This is a test" --type=INTER --domain GPN --person-name openshift-admins --person-first-name ""
```

For more information on what each parameter represent:

```shell
$ python3.8 main.py -h
usage: main.py [-h] [--kubeconfig KUBECONFIG] --command {add,remove} --landb-set-name LANDB_SET_NAME
               --label-selector LABEL_SELECTOR --landb-user USER --landb-password PASSWORD --person-name NAME
               --person-first-name FIRSTNAME --description DESCRIPTION --type TYPE --domain DOMAIN

Going to parse arguments

optional arguments:
  -h, --help            show this help message and exit
  --kubeconfig KUBECONFIG
                        path to kubeconfig file, optional if running inside cluster
  --command {add,remove}
                        Try to add/update set with add or remove set with remove
  --landb-set-name LANDB_SET_NAME
                        name of the new lanDB set to be created
  --label-selector LABEL_SELECTOR
                        labelSelector that is going to be used to get the nodes to add the set
  --landb-user USER     username to be used to preform the landb SOAP calls
  --landb-password PASSWORD
                        password to be used to preform the landb SOAP calls
  --person-name NAME    name of the person who's going to be responsible by the landb set
  --person-first-name FIRSTNAME
                        fisrtName of the person who's going to be responsible by the landb set
  --description DESCRIPTION
                        description of the landb set
  --type TYPE           type of the landb set
  --domain DOMAIN       domain of the landb set
  ```

## How it works 

After parsing the parameters, the program will initialize the SOAP client. This program uses a lib called [zeep](https://docs.python-zeep.org/en/master/) to handle SOAP calls. 

To initialize the client we have to make a call to the landDB SOAP API to get an authentication token. This is done by calling the method `getAuthToken` with the respective arguments (see [SOAP API description](https://network.cern.ch/sc/soap/6/description.html)) from this point on all calls to the SOAP API will have to have in the header the token under `Auth.token`.

After obtaining the authentication token we will try to create a K8S client using in-cluster config or the kubeconfig file. Once we create the client we will try to create the lanDB set with the desired name, if the set already exists just ignore and continue. Then we will fetch all nodes in the lanDB set (named `landb_nodes`) then, we will fetch all K8S nodes that match the `labelSelector` (named `okd_nodes`) finally, we will calculate their intersection which we call `common_nodes` which gives us nodes that we don't want to touch.

In the final steps of the script we will add all K8S nodes that are not present in `common_nodes` to the lanDB set and then we will remove all nodes that were in the lanDB set and that were not present in `common_nodes` from landDB.

### Usefull links

LanDB SOAP API description https://network.cern.ch/sc/soap/6/description.html

LanDB SOAP WSDL https://network.cern.ch/sc/soap/soap.fcgi?v=6&WSDL

Zeep doc https://docs.python-zeep.org/en/master/client.html

LanDB Sets https://landb.cern.ch/landb/portal/sets/displaySets

K8S Python lib doc https://github.com/kubernetes-client/python
