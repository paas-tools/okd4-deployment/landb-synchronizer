from zeep import Client, Settings, exceptions # pip install zeep
from kubernetes import client as k8sClient, config
import argparse
import json
import re


LANDB_SOAP_ENDPOINT = "https://network.cern.ch/sc/soap/soap.fcgi?v=6&WSDL"

client = None
auth_header = None
args = None

# Python program to illustrate the intersection 
# of two lists 
def intersection(lst1, lst2): 
	# Use of hybrid method 
	temp = set(lst2) 
	lst3 = [value for value in lst1 if value in temp] 
	return lst3 

# Initialize soap client and authentication header
def initializeClient(user, password):
    global client, auth_header

    accType="CERN"

    settings = Settings(strict=False)
    client = Client(LANDB_SOAP_ENDPOINT, settings=settings)
    token = client.service.getAuthToken(user, password, accType)
    auth_header = {'Auth': {'token': token}}

# Try to add a new set if set already exists ignore
def setInsert(landb_set_name):
    person_type = client.get_type('ns0:Person')
    person = person_type(Name=args.name, FirstName=args.firstName)

    set_info_type = client.get_type('ns0:SetInfo')
    set_info = set_info_type(ID=1, Name=landb_set_name, ResponsiblePerson=person, Description=args.description, Type=args.type, Domain=args.domain)

    try:
        client.service.setInsert(set_info, _soapheaders=auth_header)
    except exceptions.Fault as ex:
        if not ex.message.startswith("SETEXISTS"):
            raise ex

# Removes the nodes from a set and then it removes the empty set from landb
def setRemove(landb_set_name):
    # Landb only provides a method to remove empty sets, so first we have to remove
    # all nodes from the set
    landb_nodes = getSetAllAddresses(landb_set_name)
    if landb_nodes == None:
        print("Set not found on delete so set should already be deleted")    
        return

    # Remove nodes from set
    for node in landb_nodes:
        setDeleteAddress(landb_set_name, node)

    try:
        client.service.setRemove(landb_set_name, _soapheaders=auth_header) 
    except exceptions.Fault as ex:
        if ex.message.startswith("NOTFOUND"):
            print("Set not found")
        else:
            raise ex

def searchSet(landb_set_name):
    return client.service.searchSet("%"+landb_set_name+"%", _soapheaders=auth_header)

# Get all addresses in a set
def getSetAllAddresses(landb_set_name):
    try:
        return client.service.getSetAllInterfaces(landb_set_name, _soapheaders=auth_header)
    except exceptions.Fault as ex:
        if ex.message.startswith("NOTFOUND"):
            return None
        else:
            raise ex

# Add an address to a set
def setInsertAddress(landb_set_name, machineName):
    client.service.setInsertAddress(landb_set_name, machineName, _soapheaders=auth_header)

# Remove an address from a set
def setDeleteAddress(landb_set_name, machineName):
    client.service.setDeleteAddress(landb_set_name, machineName, _soapheaders=auth_header)


def main():
    global args

    # Parsing of arguments
    parser = argparse.ArgumentParser(description="Going to parse arguments")

    parser.add_argument('--kubeconfig', default="", type=str, help='path to kubeconfig file, optional if running inside cluster')

    parser.add_argument('--command', required=True, choices=['add', 'remove', 'matching-remove'], help='Try to add/update set with add command, remove set with remove command,\
         remove all sets that contain string in --landb-set-name with matching-remove')
    parser.add_argument('--landb-set-name', dest='landb_set_name', required=True, type=str, help='name of the new lanDB set to be created')
    parser.add_argument('--landb-user', dest='user', required=True, type=str, help='username to be used to preform the landb SOAP calls')
    parser.add_argument('--landb-password', dest='password', required=True, type=str, help='password to be used to preform the landb SOAP calls')
  
    parser.add_argument('--label-selector', dest='label_selector', default="", type=str, help='labelSelector that is going to be used to get the nodes to add the set')
    parser.add_argument('--person-name', dest='name', default="", type=str, help='name of the person who\'s going to be responsible by the landb set')
    parser.add_argument('--person-first-name', dest='firstName', default="", type=str, help='fisrtName of the person who\'s going to be responsible by the landb set')
    parser.add_argument('--description', type=str, default="", help='description of the landb set')
    parser.add_argument('--type', type=str, default="", help='type of the landb set')
    parser.add_argument('--domain', type=str, default="", help='domain of the landb set')
    
    args = parser.parse_args()

    # Initialize lanDB client
    initializeClient(args.user, args.password)
    
    if args.command == 'add':
        if  args.name == "" or args.description == "" or args.type == "" or args.domain == "":
            print("When using command add, you have to provide --label-selector, --person-name, \
            --person-first-name, --description, --type and --domain")
            return    

        # Initialize Openshift client
        if args.kubeconfig == "":
            config.load_incluster_config()
        else:
            config.load_kube_config(config_file=args.kubeconfig)

        cli = k8sClient.CoreV1Api()

        # Create set
        setInsert(args.landb_set_name)

        # Fetch nodes in set
        landb_nodes = getSetAllAddresses(args.landb_set_name)
        if landb_nodes == None:
            print("Set not found on create so setInsert failed silenty")    
            return

        # Fetch Openshift machines and add their names to a list named okd_nodes
        okd_nodes = []
        node_dic = cli.list_node(label_selector=args.label_selector).to_dict()    
        for machine in node_dic["items"]:
            okd_nodes.append((machine["metadata"]["name"]+".cern.ch").upper())

        # Find the intersection of landb_nodes and okd_nodes which gives us
        # nodes that we don't want to touch
        common_nodes = intersection(landb_nodes, okd_nodes)

        # Add missing nodes
        for node in okd_nodes:
            if node not in common_nodes:
                setInsertAddress(args.landb_set_name, node)

        # Remove decommissioned nodes
        for node in landb_nodes:
            if node not in common_nodes:
                setDeleteAddress(args.landb_set_name, node)


        print("Set created/updated with success")

    elif args.command == 'remove':
        
        setRemove(args.landb_set_name)

        print("Set removed with success")

    elif args.command == 'matching-remove':
        setsNamesToRemove = searchSet(args.landb_set_name)

        removed = 0
        for name in setsNamesToRemove:            
            matched = re.match("[A-Z ]*[ ]" + args.landb_set_name.upper() + "[ ][A-Z ]*", name)
            if bool(matched): 
                setRemove(name)
                removed += 1
        
        print(str(removed) + " sets that match expression were removed")

    else:
        print("Invalid command")


if __name__ == "__main__":
    main()